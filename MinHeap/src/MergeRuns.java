import java.io.*;

public class MergeRuns {

	public static void main(String[] args) throws Exception {
		
		// the number of nodes in the array
		int howManyNodes = Integer.parseInt(args[0]);
		
		// These variables deal with the input text file
		FileReader fromRunFile = new FileReader(args[1]);
		BufferedReader bReader = new BufferedReader(fromRunFile);
		
		// Captures the array describing the information about the runs
		String line = bReader.readLine();
		while (!line.contains("*"))	{
			if (line.contains("*")) {
				return;
			}
			line += bReader.readLine();
		}
		// performs a replacement of the non digits, then with leading commas, and ending commas
		line = line.replaceAll("[^\\d]", ",").replaceAll("^,+", "").replaceAll(",+$", "");
		String[] runsInfoString = line.split(",");
		int[] runsInfo = new int[runsInfoString.length];
		// Converts the string captured information to integers
		for (int i = 0; i < runsInfoString.length; i++) {
			runsInfo[i] = Integer.parseInt(runsInfoString[i]);
		}
		
		// create files for each of the runs
		for (int i = 1; i < runsInfo.length; i++) {
			String filename = "run" + i + ".txt";
			// creates the files for each of the runs
			PrintWriter pWriter = new PrintWriter(filename);
		}
		
		for (int j = 0; j < runsInfo.length; j += howManyNodes) {
		
			// check to see what the maximum necessary size of the heap is
			if (runsInfo.length < howManyNodes) {
				howManyNodes = runsInfo.length;
			}
			
			NodeData[] list = new NodeData[howManyNodes];
			
			// sets a value for last index as it is a better variable
			int lastIndex = howManyNodes-1;
			
			// create an array of readers which will hold the position of the readline
			BufferedReader[] readers = new BufferedReader[howManyNodes];
			
			// keep an account of the current run
			Integer runIndex = 0;
			// input the value of the runs into each of the files
			while (runIndex < runsInfo.length-1) {
				// to which file will we be writing too
				File outfile = new File("run"+(runIndex+1)+".txt");
				FileWriter fWriter = new FileWriter(outfile, true);
				PrintWriter pWriter = new PrintWriter(fWriter);
				
				// write all information that is pertinent to the run
				for (int i = runsInfo[runIndex]; i < runsInfo[runIndex+1]; i++) {
					pWriter.println(bReader.readLine());
				}		
				
				// increment the run that we're dealing with
				runIndex++;
				// close the PrintWriter for this run
				pWriter.close();
			}
			
			// Add the information to the last run 
			File outfile = new File("run" + (j+howManyNodes)+".txt");
			FileWriter fWriter = new FileWriter(outfile, true);
			PrintWriter pWriter = new PrintWriter(fWriter);
			
			while (line != null) {
				line = bReader.readLine();
				if (line == null) {
					break;
				}
				pWriter.println(line);
			}		
			pWriter.close();
			
			// These variables deal with the output text file
			String outputFile = args[1].replaceAll(".runs.txt", ".sorted.txt");
			outfile = new File(outputFile);
			fWriter = new FileWriter(outfile, true);
			pWriter = new PrintWriter(fWriter);
			
			// Fill the array (the heap)
			for (int i = 0; i < howManyNodes; i++) {
				FileReader whichFile = new FileReader("run"+(j+i+1)+".txt");
				readers[i] = new BufferedReader(whichFile);	
				list[i] = new NodeData(readers[i].readLine(), i);
			}
			
			System.out.println(howManyNodes+j);
			FileReader whichFile = new FileReader("run"+(howManyNodes+j)+".txt");
			readers[lastIndex] = new BufferedReader(whichFile);
			list[lastIndex] = new NodeData(readers[lastIndex].readLine(), lastIndex);
			
			// Create the heap		
			Heap heap = new Heap(list, howManyNodes);
			
			// iterate through the heap and output the smallest node
			try {
				while (howManyNodes > 0) {
					NodeData node = heap.remove();
					pWriter.println(node.input);
					line = readers[node.index].readLine();
					
					if (line != null) {
						heap.add(new NodeData(line, node.index));
					}
					else {
						howManyNodes--;
					}
				}
			}
			finally { }
			
			// when there are no more things to be input from the runs
			// we output all values that remain in the heap
			for (int i = heap.list.size(); i > 0; i--) {
				pWriter.println(heap.remove().input);
			}
			
			for (int i = 0; i < readers.length; i++) {
				readers[i].close();
			}
			
			pWriter.close();
		}
		
		// close the BufferedReader and PrintWriter to finish the program
		bReader.close();
		
			
	}
}