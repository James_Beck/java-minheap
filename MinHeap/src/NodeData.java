public class NodeData {
	public String input;
	public int index;
	
	public NodeData(String input, int index) {
		this.input = input;
		this.index = index;
	}
}
