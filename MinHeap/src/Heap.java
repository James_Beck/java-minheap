import java.util.ArrayList;

// The generic 'Node' is being used as it is the most appropriate type name for visualisation
public class Heap {
	public ArrayList<NodeData> list = new ArrayList<>();
	
	/** Create a default heap */
	public Heap() {
	}
	
	/** Create a heap from an array of objects */
	public Heap(NodeData[] nodes, Integer nodeLength) {
		for (int i = 0; i < nodeLength; i++) {
			add(nodes[i]);
		}
	}
	
	/** Add a new object into the heap */
	public void add(NodeData node) {
		// Append the new node to the heap
		list.add(node);
		// get the index of the last node in preparation for a heapify
		int currentIndex = list.size() - 1;
		
		// perform a loop while we are dealing with nodes that are not the root node
		while (currentIndex > 0) {
			// get the value of the parent node
			int parentIndex = (currentIndex - 1) / 2;
			String parentValue = list.get(parentIndex).input;
			String childValue = list.get(currentIndex).input;
			
			// Swap if the current nodes input value is less than that of its parent
			// reads as if new node < parent node perform swap operation
			if (childValue.compareTo(parentValue) < 0) {
				NodeData temp = list.get(currentIndex);
				list.set(currentIndex, list.get(parentIndex));
				list.set(parentIndex, temp);
			}
			else {
				break; // The tree is a heap now
			}
		
			// Changes the node that we are comparing to the parent node to re-test min value against new parent
			currentIndex = parentIndex;
		}
	}

	/** Remove the root from the heap */
	public NodeData remove() {
		// If the list contains nothing, then we do have the ability to remove anything
		if (list.size() == 0) {
			return null;
		}
		
		// capture the value of the root node
		NodeData removedNode = list.get(0);
		// replace the value of the root node with the value of the last node
		list.set(0, list.get(list.size() - 1));
		// delete the last node so to remove the newly created duplicate in the tree
		list.remove(list.size() - 1);
	
		// Set the current index to the root node
		int currentIndex = 0;
		
		// while loop to perform the sift down process as many times as we find the root node
		// to be larger than the value of it's smallest child
		// This is to be terminated when we reach the last node of the tree
		while (currentIndex < list.size()) {
			// get the value for the children of the node
			int leftChildIndex = 2 * currentIndex + 1;
			int rightChildIndex = 2 * currentIndex + 2;
	
			// Find the maximum between two children
			if (leftChildIndex >= list.size()) {
				break; // The tree is a heap
			}
			// Make a default value for the Min Index
			Integer minChildIndex = leftChildIndex;
			String minChildValue = list.get(leftChildIndex).input;
			// If we then find that the rightChild is smaller than the left child, we change the
			// value of the min child to equal right child
			if (rightChildIndex < list.size()) {
				String rightChildValue = list.get(rightChildIndex).input;
				if (minChildValue.compareTo(rightChildValue) > 0) {
					minChildIndex = rightChildIndex;
					minChildValue = rightChildValue;
				}
			}
			
			// Capture the value of the parent node
			String parentValue = list.get(currentIndex).input;
	
			// If the parent node is greater than the smallest of its child nodes
			if (parentValue.compareTo(minChildValue) > 0) {
				// store the value of the smallest child in a temporary node
				NodeData temp = list.get(minChildIndex);
				// swap the value of the minimum child with its parent
				list.set(minChildIndex, list.get(currentIndex));
				// set the value of the parent to the value of smallest child
				list.set(currentIndex, temp);
				// set the node of interest to the newly positioned child node
				currentIndex = minChildIndex;
			}
			else {
				break; // The tree is a heap
			}
		}
		
		// the value of the root node that was removed and will be placed within the text document
		return removedNode;
	}
	 // allows for reading the console information
	@Override
	public String toString()
	{
		return "List: " + list.toString();
	}
}
